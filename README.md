# Reduced UWS evaporation and decomposition model

This library inlcudes a reduced UWS evaporation and decomposition model. It
tracks the progress of single UWS droplets during evaporation and decomposition
with its properties:

- Droplet temperature
- (D/D_0)^2
- Density rho
- Total mass
- w_h2o (mass fraction of water)
- w_urea_s 
- w_urea_l
- w_hnco_l
- w_biu_l
- w_biu_s
- w_triu
- w_cya_s
- w_ammd_s

## Usage

Initialize table:

`
drop_table = uws_model.droplet.Table()
`

Initialize droplet with table, initial radius and ambient temperature

`
drop = uws_model.droplet.Droplet(drop_table, r0, T0)
`

The droplet properies can be returned with the *get_data* method:

`
[ d2, 
  temp, 
  rho, 
  m_tot, 
  w_h2o, 
  w_ur_s, 
  w_ur_l, 
  w_hnco_l, 
  w_biu_l,
  w_biu_s,
  w_cya_s,
  w_ammd_s] = drop.get_data()
`

When time-stepping, the mass-flows from the droplet are returned:

`
[md_h2o, md_nh3, md_cya, md_ammd, md_ur, md_hnco, qgas] = drop.time_step(delta_t)
`

A sample calculation is in the tests directory
