import numpy as np

from uws_model.droplet import Table, Droplet

# Define initial radius and ambient temprature
r0 = 3.25e-5
T0 = 700.0

# Initialize table
drop_table = Table()

# Initialize droplet
drop = Droplet(drop_table, r0, T0)

# Create time array
nup = 1000
t = np.linspace(0, 10, nup+1)

# Create arrays for properties
d2 = np.zeros(nup)
temp = np.zeros(nup)
rho = np.zeros(nup)
m_tot = np.zeros(nup)
w_h2o = np.zeros(nup)
w_ur_s = np.zeros(nup)
w_ur_l = np.zeros(nup)
w_hnco_l = np.zeros(nup)
w_biu_l = np.zeros(nup)
w_biu_s = np.zeros(nup)
w_triu = np.zeros(nup)
w_cya_s = np.zeros(nup)
w_ammd_s = np.zeros(nup)

# Create arrays for mass flows
md_h2o = np.zeros(nup)
md_nh3 = np.zeros(nup)
md_cya = np.zeros(nup)
md_ammd = np.zeros(nup)
md_ur = np.zeros(nup)
md_hnco = np.zeros(nup)
qgas = np.zeros(nup)

# Do time-stepping, save droplet properties, calculate mass flows
for i in range(nup):
    dt = t[i+1] - t[i]
    dat1 = drop.get_data()
    d2[i] = dat1[0]
    temp[i] = dat1[1]
    rho[i] = dat1[2]
    m_tot[i] = dat1[3]
    w_h2o[i] = dat1[4]
    w_ur_s[i] = dat1[5]
    w_ur_l[i]= dat1[6]
    w_hnco_l = dat1[7]
    w_biu_l[i] = dat1[8]
    w_biu_s[i] = dat1[9]
    w_triu[i] = dat1[10]
    w_cya_s[i] = dat1[11]
    w_ammd_s[i] = dat1[12]

    # Do time-stepping
    dat2 = drop.time_step(dt)
    md_h2o[i] = dat2[0]
    md_nh3[i] = dat2[1]
    md_cya[i] = dat2[2]
    md_ammd[i] = dat2[3]
    md_ur[i] = dat2[4]
    md_hnco[i] = dat2[5]
    qgas[i] = dat2[6]

