import numpy as np
import os

cdir = os.path.dirname(__file__)
rrange = np.load(os.path.join(cdir, './rrange.npy'))
trange = np.load(os.path.join(cdir, 'trange.npy'))
table = np.load(os.path.join(cdir, 'table.npy'))
