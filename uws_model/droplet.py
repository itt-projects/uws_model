import os
import numpy as np
from scipy.integrate import quad
from uws_model import *
import uws_model.tables

class Table:
    """
    This class loads, when initialized the rrange.npy trange.npy and table.npy
    files to which the parametrized data for the uws model has been saved.

    The class method table_par( r0, T0) returns the parameter table for given
    far field temperatures T0 and initial droplet radius r0.

    """
    def __init__(self):
        self.rrange = uws_model.tables.rrange
        self.trange = uws_model.tables.trange
        self.data = uws_model.tables.table

    def table_par(self, r0, T0):
        # 1) Find r0 neighbours
        if r0 < self.rrange[0]:
            i1 = 0
            i2 = 1
        elif r0 > self.rrange[-1]:
            i2 = len(self.rrange) - 1
            i1 = i2 - 1
        else:
            i = np.absolute(self.rrange - r0).argmin()
            if self.rrange[i] - r0 < 0 and i != len(self.rrange)-1:
                i1 = i
                i2 = i + 1
            else:
                i1 = i - 1
                i2 = i

        # 2) Find T0 neighbours
        if T0 < self.trange[0]:
            j1 = 0
            j2 = 1
        elif T0 >= self.trange[-1]:
            j2 = len(self.trange) - 1
            j1 = j2 - 1
        else:
            j = np.absolute(self.trange - T0).argmin()
            if self.trange[j] - T0 > 0 and j != len(self.trange)-1:
                j1 = j
                j2 = j + 1
            else:
                j1 = j - 1
                j2 = j
        # Return table for parameters
        return 1/(self.rrange[i2] - self.rrange[i1])/\
                (self.trange[j2] - self.trange[j1])*\
                (self.data[:,:,j1,i1] * (self.rrange[i2] - r0)\
                * (self.trange[j2] - T0) +\
                self.data[:,:,j1,i2] * (r0 - self.rrange[i1])\
                * (self.trange[j2] - T0) + \
                self.data[:,:,j2,i1] * (self.rrange[i2] - r0)\
                * (T0 - self.trange[j1]) + \
                self.data[:,:,j2,i2] * (r0 - self.rrange[i1])\
                * (T0 - self.trange[j1]))


class Droplet:
    """
    Droplet class. Can be initialized using an initialized Table a
    far field temperature T0 and initial radius r0.

    Using the delta_t(time) method, the droplet is progressed by time
    """

    def __init__(self, table, r0, T0):
        self.table = table.table_par(r0, T0)
        self.phi = self.table[-1,0]

        # Create interpolators (lambda functions) for droplet properties
        self.d2 = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,2])
        self.temp = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,4])
        self.rho = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,5])
        self.m_tot = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,3])
        self.w_h2o = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,6])
        self.w_ur_s = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,7])
        self.w_ur_l = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,8])
        self.w_hnco_l = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,9])
        self.w_biu_l = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,10])
        self.w_biu_s = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,11])
        self.w_triu = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,12])
        self.w_cya_s = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,13])
        self.w_ammd_s = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,14])

        # Create interpolators for mass and heat flows
        self.md_h2o = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,15])
        self.md_nh3 = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,16])
        self.md_cya = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,17])
        self.md_ammd = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,18])
        self.md_ur = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,19])
        self.md_hnco = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,20])
        self.qgas = lambda phii:\
            np.interp(phii, self.table[:,0], self.table[:,21])

    def get_data(self):
        phi = self.phi
        dat = np.array([self.d2(phi), self.temp(phi), self.rho(phi),
                        self.m_tot(phi), self.w_h2o(phi), self.w_ur_s(phi),
                        self.w_ur_l(phi), self.w_hnco_l(phi),
                        self.w_biu_l(phi), self.w_biu_s(phi),
                        self.w_triu(phi), self.w_cya_s(phi),
                        self.w_ammd_s(phi)])
        return dat

    def time_step(self, delta_t):
        phi = self.phi
        phidt = np.interp(phi, self.table[:,0], self.table[:,1])
        phi_new = phi + phidt*delta_t

        # Calculate mass and hear flows in range
        md_h2o_dt, e = quad(self.md_h2o, phi, phi_new)
        md_nh3_dt, e = quad(self.md_nh3, phi, phi_new)
        md_cya_dt, e = quad(self.md_cya, phi, phi_new)
        md_ammd_dt, e = quad(self.md_ammd, phi, phi_new)
        md_ur_dt, e = quad(self.md_ur, phi, phi_new)
        md_hnco_dt, e = quad(self.md_hnco, phi, phi_new)
        qgas, e = quad(self.qgas, phi, phi_new)

        # Update phi
        self.phi = phi_new

        return np.array([md_h2o_dt, md_nh3_dt, md_cya_dt, md_ammd_dt, md_ur_dt,
                         md_hnco_dt, qgas])

