from setuptools import setup, find_packages

VERSION = '1.0'
DESCRIPTION = 'Tabulated UWS model'
LONG_DESCRIPTION = 'Tabulated model for UWS evaporation and decomposition'

# Setting up
setup(
        name="uws_model",
        version=VERSION,
        author="Etele Berszany",
        author_email="etele.berszany@kit.edu",
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        packages=find_packages(),
        install_requires=[])
